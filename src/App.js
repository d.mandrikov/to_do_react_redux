import './App.css';
import TodoInput from './components/TodoInput/TodoInput';
import TodoList from './components/TodoList/TodoList';
import 'bootstrap/dist/css/bootstrap.css'

function App() {
  return (
    <div className="App">
      <div className="header">
        <p>Список дел</p>
        <TodoInput/>
      </div>
      <TodoList/>
    </div>
  );
}

export default App;
