# about

This is a to do list application implemented on react and redux stack.

## Install dependencies

Before run the project use a command "**npm install**" to install dependencies.

### Run app

The "**yarn start**" command runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
