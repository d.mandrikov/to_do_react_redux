import React from "react";
import { addTodo } from "../../store/actions";
import "./TodoInput.css";
import { v1 as uuid } from "uuid";
import { useDispatch } from "react-redux";

const TodoInput = () => {
  const dispatch = useDispatch();

  const handlerClick = (event) => {
      event.preventDefault();
        let data = event.target.elements;
        if (data.taskName.value !== '') {
        dispatch(addTodo({
        id: uuid(),
        name: data.taskName.value,
        isDone: false,
      }));
        data.taskName.value = '';
    }
    
  };
  
  return (
    <form onSubmit={handlerClick}>
      <input
        type="text"
        placeholder={"Название..."}
        name = 'taskName'
      />
      <button type="submit" className={"addBtn"}>
        Добавить
      </button>
    </form>
  );
};
export default TodoInput;
