import React from "react";
import { useDispatch } from "react-redux";
import { deleteTodo, doneTodo } from "../../store/actions";
import "./TodoItem.css";

const TodoItem = ({ todo }) => {
  const className = "p " + (todo.isDone ? "checked" : "");

  const dispatch = useDispatch();
  const handleClickDone = () => dispatch(doneTodo(todo.id))

  const handlerClickDelete = () => dispatch(deleteTodo(todo.id));
  
  return (
    <div className="task">
      <p className={className} onClick={handleClickDone}>
        {todo.name}
        <span className={"close"} onClick={handlerClickDelete}>
          x
        </span>
      </p>
    </div>
  );
};
export default TodoItem;
