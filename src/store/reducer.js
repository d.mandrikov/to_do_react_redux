import {ADD_TODO, DELETE_TODO, DONE_TODO} from './actions'
import {initialState} from './initialState'



export let reducer = (state = initialState, action) => {
    const {todos} = state;
    let newTodos;
    switch (action.type) {
        case ADD_TODO:
            newTodos = todos.map(item => item)
            newTodos.push(action.payload)
            return {
                ...state,
                todos: newTodos
                };

        case DELETE_TODO:
            return {
                ...state,
                todos: todos.filter(todo => todo.id !== action.payload)
            }
        
        case DONE_TODO:
            return {
                ...state,
                todos: state.todos.map(todo => {
                    if (todo.id === action.payload) {
                      return { ...todo,
                        isDone: !todo.isDone
                      }
                    } else {
                      return todo;
                    }
                })
            }
    
        default:
            return state;
    }
}